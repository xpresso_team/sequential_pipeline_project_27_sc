## Default Makefile generated for the  project : echo_task
##


branch_name=`git branch | grep \* | cut -d ' ' -f2`
branch_name_str=`git branch | grep \* | cut -d ' ' -f2 | tr -d '/\ '`

build_folder=${CURDIR}/xprbuild
docker_build_folder=${build_folder}/docker
k8_build_folder=${build_folder}/docker
linux_build_folder=${build_folder}/system/linux
windows_build_folder=${build_folder}/system/windows

export PROJECT_VERSION=`cat VERSION`
export ROOT_FOLDER=${CURDIR}
export PROJECT_NAME=echo_task

default: debug

# Clean the projects
clean:
	@echo "--------------- Clean Started ----------------"
	mvn clean
	@echo "--------------- Clean Completed ----------------"

clobber: clean
	@echo "--------------- Clobber Started ----------------"
	rm -rf target/
	docker rm -f yum-pms
	@echo "--------------- Clobber Completed ----------------"

# Much needed git commands
checkout:
	git pull

update: clean checkout

push: clean
	git squash
	git push origin ${branch_name}

patch: clean
	@if [ -z "${ARGS}" ];then\
		echo "\nError: Pass remote branch for creating patch.\n--usage: make patch master";\
	else\
		echo  "Creating patch for following commits";\
		git log --pretty=oneline -1;\
		git format-patch ${ARGS} --stdout > ${branch_name_str}.patch;\
	fi;

# Perform test
lint:
	@echo "--------------- Quality Check Started ----------------"
	@echo "Sonar Project Tester"
	@echo "--------------- Quality Check Completed ----------------"

unittest:
	@echo "Performing Unit Test"
	mvn test

apitest:
	@echo "Performing API Testing"
	/bin/bash  ${docker_build_folder}/test.sh

systemtest:
	@echo "Performing System Testing"

test-all: unittest apitest systemtest

# Build
dependency:
	@echo "Installing Dependencies"
	/bin/bash ${docker_build_folder}/pre-build.sh

debug: clean
	@echo "Building docker"
	/bin/bash ${docker_build_folder}/build.sh debug

release: clobber dependency
	@echo "Create release docker image"
	/bin/bash ${docker_build_folder}/build.sh release

module: clean
	@echo "Creates modules which can be distributed across using pypi repository"

all: debug release

build: debug

dist: clobber
	@echo "Generate distribution"

# Train
train:
	@echo "Training the datacode"

# Deployment
deploy-local: run

deploy-debug: debug-docker run-docker
	@echo "Run docker container in debug mode"
	python ${k8_build_folder}/generate-yaml.py

deploy-release: release
	@echo "Deploy to kubernetes"
	/bin/bash ${k8_build_folder}/service-deploy.sh

run:
	@echo "Running deploy local application"
	/bin/bash ${docker_build_folder}/deploy.sh

# utils
doc:
	@echo "This should generate the docs"

version:
	@echo "This should print the version"
	echo "${PROJECT_VERSION}"

.PHONY:
	clean all debug debug-local release deploy deploy-local run run-local
